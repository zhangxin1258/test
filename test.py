import time
import threading
import random
from pymodbus.version import version
from pymodbus.server.sync import StartTcpServer
from pymodbus.server.sync import StartTlsServer
from pymodbus.server.sync import StartUdpServer
from pymodbus.server.sync import StartSerialServer
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock, ModbusSparseDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.transaction import ModbusRtuFramer, ModbusBinaryFramer
# --------------------------------------------------------------------------- #

def run_server(host, port, value):

    store = ModbusSlaveContext(
        # di=ModbusSequentialDataBlock(0, [3]*500),  # 数字输入
        # co=ModbusSequentialDataBlock(0, [3]*500),  # 线圈
        hr=ModbusSequentialDataBlock(0, [3]*500),  # 保持寄存器
        # ir=ModbusSequentialDataBlock(0, [3]*500),  # 输入寄存器
        )
    context = ModbusServerContext(slaves=store, single=True)
    identity = ModbusDeviceIdentification()
    identity.VendorName = 'Pymodbus'
    identity.ProductCode = 'PM'
    identity.VendorUrl = 'http://github.com/riptideio/pymodbus/'
    identity.ProductName = 'Pymodbus Server'
    identity.ModelName = 'Pymodbus Server'
    identity.MajorMinorRevision = version.short()

    StartTcpServer(context, identity=identity, address=(host, port))

if __name__ == "__main__":
    threads = []
    for i in range(502, 511):
        threads.append(threading.Thread(target=run_server, args=('', i, i)))
    for t in threads:
        print(t)
        t.start()